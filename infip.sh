#!/bin/bash

source Scripts/init.sh
source Scripts/configManager.sh
source Scripts/recordManager.sh

OPTION=$1	# option provided by the user

show_valid_option()
{
	echo "Provide valid option"
        cat .options
}

if [ "${OPTION}" == "-init" ]	# this block will initialize the basic configuration and install additional packeges that it requires to use
then
	if [ $# -eq 3 ]
	then	watch_path=$2
		record_path=$3
		echo "initializing..............."
		install_iwatch		# func path --- Scripts/init.sh | this will install iwatch
		if [ $? -eq 0 ]
		then
			echo "installed iwatch."
			echo "initializing config file..............."
       			initialize_conf	"${watch_path}" "${record_path}"	# func path --- Scripts/init.sh | this will initialize config file
			echo "setting up record dir..............."
			initialize_record 	# func path --- Scripts/iniit.sh | this will set up records
			echo "set up complete.............."
		else
       			echo "Terminated"
		fi
	else
		show_valid_option
	fi
elif [ "${OPTION}" == "-start" ]	#this block will start the watch using iwatch
then
	echo "Starting watch............"
	bash Scripts/watchStarter.sh
	echo "Watch started..........."
elif [ "${OPTION}" == "-show" ]		#this block will display the records
then
	cmd=$(get_record)	# func path --- Scripts/recordManager.sh | this will get the command to print all records
	watch -t -n 1 ${cmd}
else
	show_valid_option
fi
