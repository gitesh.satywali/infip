#!/bin/bash

#This file will initialize watch command
#author="Gitesh Satywali"

#all the required sources
source Scripts/configManager.sh

#required variables
PATH_TO_SCRIPTS=$(get_conf "${SCRIPT_PATH}")
PROCESS_SCRIPT_PATH="${PATH_TO_SCRIPTS}/processFile.sh"
PATH_TO_WATCH=$(get_conf "${WATCH_PATH}")

#this will start the iwatch for defined paths
iwatch -c "bash ${PROCESS_SCRIPT_PATH} %f %e" "${PATH_TO_WATCH}"
