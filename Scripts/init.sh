#!/bin/bash

source Scripts/configManager.sh

# This will check if iwatch is installed and install it if needed

install_iwatch() {
    if ! dpkg -l | grep "iwatch" > /dev/null 2>&1
    then
	    echo "installing iwatch.........."
	    sudo apt-get install iwatch -y > /dev/null 2>&1
    else
	    echo "iwatch already installed."
    fi
}

# This will ask user to define the necessary config for infip such as path to watch and path to store records and initialize the conig file
initialize_conf()
{
	TO_BE_WATCHED=$1
	TO_BE_RECORD_PATH=$2
	if [[ -z "${TO_BE_WATCHED}" || -z "${TO_BE_RECORD_PATH}" ]]	# This will check if the input is provided
	then
        	echo "No input specified"
	elif [[ -e "${TO_BE_WATCHED}" && -e "${TO_BE_RECORD_PATH}" ]]	# This will check if the paths provided are valid
	then
        	TO_BE_SCRIPT_PATH=$(readlink -f .)	# gets the path where script is running
        	create_conf_file "${TO_BE_WATCHED}" "${TO_BE_RECORD_PATH}" "${TO_BE_SCRIPT_PATH}/Scripts"  # func path --- Scripts/configManaager.sh
        	touch "${TO_BE_RECORD_PATH}/record"
        	chmod 777 "${TO_BE_RECORD_PATH}/record"
	else
        	echo "Provide valid path"
	fi
}

# This will create a file named record in the mentioned record directory
initialize_record()
{
	REC_PATH=$(get_conf ${RECORD_PATH})
	touch "${REC_PATH}/record"
        chmod 777 "${REC_PATH}/record"
}
