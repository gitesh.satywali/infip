#!/bin/bash

#This file will manage all the handlings of records related to the process
#author="Gitesh Satywali"

#all the required sources
source "Scripts/configManager.sh"

#required variables
PATH_TO_RECORDS=$(get_conf "${RECORD_PATH}")
IN_PROC="PROCESSING"
IN_RECEIV="RECEIVING"
ON_PROC="PROCESSED"

#required functions

#this function will get the record directory dedicated for this file basically returns the file name without extensions
get_dir_for_record()
{
	FILE_NAME=$1		#- sam_123.log
	DIR_NAME=$(echo "${FILE_NAME}" | cut -d '.' -f 1)
        RECORD_NAME="${PATH_TO_RECORDS}/${DIR_NAME}"
	readlink -f "${RECORD_NAME}"
}

#this function will create a seprate directory for a record of perticular file
create_new_record()
{
	FILE_NAME=$1		#ex- sam_123.log
	RECORD_NAME=$(get_dir_for_record "${FILE_NAME}")
	mkdir "${RECORD_NAME}"
	touch "${RECORD_NAME}/record"
	chmod 777 "${RECORD_NAME}"
	chmod 777 "${RECORD_NAME}/record"
}

#this function will update the record 
update_record()
{
	FILE_NAME=$1		#ex- sam_123.log
	FILE_STATUS=$2		#ex- PROCESSING
	RECORD_STATUS=$3	#ex- 5/123
	RECORD_NAME=$(get_dir_for_record "${FILE_NAME}")
	if [ "${FILE_STATUS}" == "${IN_RECEIV}" ]
	then
		$(create_new_record "${FILE_NAME}")
		echo -e "${FILE_NAME}\t${FILE_STATUS}\t----" > "${RECORD_NAME}/record"
	elif [ "${FILE_STATUS}" == "${IN_PROC}" ]
	then	
		echo -e "${FILE_NAME}\t${FILE_STATUS}\t${RECORD_STATUS}" > "${RECORD_NAME}/record"
	elif [ "${FILE_STATUS}" == "${ON_PROC}" ]
	then
		echo -e "${FILE_NAME}\t${FILE_STATUS}\t${RECORD_STATUS}" >> "${PATH_TO_RECORDS}/record"
		rm -rf "${RECORD_NAME}"
	fi
}

# this function will return the command to show record
get_record()
{
	echo -e "(find \"${PATH_TO_RECORDS}\" -mindepth 2 -maxdepth 2 -type f -exec cat {} + && tac \"${PATH_TO_RECORDS}/record\") | sed -e '1i\---FILE_NAME--- ---FILE_STATUS--- ---RECORD_STATUS---' | column -t"
}
