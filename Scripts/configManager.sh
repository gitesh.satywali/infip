#!/bin/bash

#this file will help in manging configuration
#author  Gitesh Satywali

CONF_FILE_PATH="infip.conf"

WATCH_PATH="PATH_TO_WATCH"
SCRIPT_PATH="PATH_TO_SCRIPT"
RECORD_PATH="PATH_TO_RECORD"


# This function will create a new config file for infip and initialize it with necessary configuration
create_conf_file()
{
	PATH_TO_WATCH=$1
        PATH_TO_RECORD=$2
        PATH_TO_SCRIPT=$3
        rm -rf "${CONF_FILE_PATH}" 	# Removed old config file
        touch "${CONF_FILE_PATH}" 	# Added new config file
        echo -e "#\n# This file will contain the configuration for infip" >> "${CONF_FILE_PATH}"
        add_new_conf "${WATCH_PATH}" "${PATH_TO_WATCH}" "The path that infip will keep an eye on"
        add_new_conf "${SCRIPT_PATH}" "${PATH_TO_SCRIPT}" "The path where scripts are located"
        add_new_conf "${RECORD_PATH}" "${PATH_TO_RECORD}" "The path that will store the records"
}

# This function will add a new config in config file
add_new_conf()
{
        VAR_NAME=$1
        VAR_VALUE=$2
        VAR_DESC=$3
        echo -e "#\n# ${VAR_DESC}" >> "${CONF_FILE_PATH}" 	# Adding description for config
        echo "${VAR_NAME}=${VAR_VALUE}" >> "${CONF_FILE_PATH}"
}

# This function will return the config value
get_conf(){
	CONF_NAME=$1
	grep "${CONF_NAME}" "${CONF_FILE_PATH}" | cut -d'=' -f 2
}
