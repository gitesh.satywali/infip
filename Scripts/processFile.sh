#!/bin/bash

#this will process a file
#author Gitesh Satywali

source Scripts/recordManager.sh

FILE_PATH=$1		#ex- /srv/infip/AllFiles/sam_123.log
EVENT=$2		#ex- IN_CREATE
FILE_NAME=$(echo ${FILE_PATH} | rev | cut -d'/' -f1 | rev)

#function to read line by line and do stuff
#your code goes here for processing it line by line
process_lines()
{
	COUNTED_LINES=$1
	count=1
	while read line
	do
		update_record "${FILE_NAME}" "${IN_PROC}" "${count}/${COUNTED_LINES}"	#modify record for each line
		count=$((${count}+1))
	done < "${FILE_PATH}"

	if [ "${DESCRIBED_LINES}" == "${COUNTED_LINES}" ]	#dummy to update record after every file is processed
        then
                update_record "${FILE_NAME}" "${ON_PROC}" "SUCCESS"
        else
                update_record "${FILE_NAME}" "${ON_PROC}" "FAIL"
        fi
}

if [ "${EVENT}" == "IN_CREATE" ]	# this block will run when a new file is received by the directory
then
	update_record "${FILE_NAME}" "${IN_RECEIV}"	
elif [ "${EVENT}" == "IN_CLOSE_WRITE" ]	# this block will run when a file is written completely
then
	# dummy proceeding
	DESCRIBED_LINES=$(echo "${FILE_NAME}" | awk -F '[_.]' '{print $2}')
	COUNTED_LINES=$(wc -l ${FILE_PATH} | cut -d ' ' -f1)
	process_lines "${COUNTED_LINES}" &
fi
